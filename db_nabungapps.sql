/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.4.14-MariaDB : Database - db_nabungapps
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_nabungapps` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `db_nabungapps`;

/*Table structure for table `log_user` */

DROP TABLE IF EXISTS `log_user`;

CREATE TABLE `log_user` (
  `log_id` varchar(25) DEFAULT NULL,
  `log_type` enum('TAMBAH','KURANG','UPDATE') DEFAULT NULL,
  `kd_aksi` varchar(7) DEFAULT NULL,
  `kd_type` enum('NABUNG','WISHLIST','AMBIL','AKUN') DEFAULT NULL,
  `nominal_uang` int(10) DEFAULT NULL,
  `tgl_dibuat` datetime DEFAULT NULL,
  `pesan` varchar(255) DEFAULT NULL,
  `id_user` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `log_user` */

insert  into `log_user`(`log_id`,`log_type`,`kd_aksi`,`kd_type`,`nominal_uang`,`tgl_dibuat`,`pesan`,`id_user`) values 
('LOG0001','TAMBAH','NBG0001','NABUNG',15000,'2021-01-18 05:30:31','Tabungan telah ditambahkan','USR0001'),
('LOG0002','TAMBAH','NBG0002','NABUNG',20000,'2021-01-18 05:30:40','Tabungan telah ditambahkan','USR0001'),
('LOG0003','UPDATE','NBG0002','NABUNG',15000,'2021-01-18 05:50:45','Nominal Tabungan Telah diubah','USR0001'),
('LOG0004','KURANG','TNB0001','AMBIL',1000,'2021-01-18 05:52:54','Tabungan telah diambil','USR0001'),
('LOG0005','TAMBAH','NBG0004','NABUNG',250000,'2021-01-20 23:01:22','Tabungan telah ditambahkan','USR0001'),
('LOG0006','UPDATE','NBG0004','NABUNG',25000,'2021-01-20 23:02:00','Nominal Tabungan Telah diubah','USR0001'),
('LOG0007','UPDATE','USR0001','AKUN',0,'2021-01-22 20:23:41','Akun Password Telah di Update','USR0001'),
('LOG0008','UPDATE','USR0001','AKUN',0,'2021-01-22 20:24:20','Akun Password Telah di Update','USR0001'),
('LOG0009','TAMBAH','NBG0005','NABUNG',120000,'2021-01-22 20:26:56','Tabungan telah ditambahkan','USR0001'),
('LOG0010','UPDATE','NBG0005','NABUNG',12000,'2021-01-22 20:27:04','Nominal Tabungan Telah diubah','USR0001'),
('LOG0011','TAMBAH','NBG0006','NABUNG',250000,'2021-01-23 01:38:44','Tabungan telah ditambahkan','USR0001'),
('LOG0012','UPDATE','NBG0006','NABUNG',25000,'2021-01-23 01:38:56','Nominal Tabungan Telah diubah','USR0001'),
('LOG0013','UPDATE','NBG0006','NABUNG',14500,'2021-01-23 01:39:11','Nominal Tabungan Telah diubah','USR0001'),
('LOG0014','UPDATE','USR0001','AKUN',0,'2021-01-23 01:46:19','Akun Password Telah di Update','USR0001'),
('LOG0015','KURANG','TNB0001','AMBIL',500,'2021-01-23 17:02:55','Tabungan telah diambil','USR0001'),
('LOG0016','UPDATE','USR0001','AKUN',0,'2021-01-27 18:19:36','Akun Password Telah di Update','USR0001'),
('LOG0017','TAMBAH','NBG0008','NABUNG',15000,'2021-01-29 08:48:34','Tabungan telah ditambahkan','USR0001');

/*Table structure for table `tb_nabung` */

DROP TABLE IF EXISTS `tb_nabung`;

CREATE TABLE `tb_nabung` (
  `kd_nabung` varchar(7) NOT NULL,
  `nominal_uang` int(10) NOT NULL,
  `tgl_nabung` varchar(25) NOT NULL,
  `id_user` varchar(7) NOT NULL,
  `status` enum('+','-') NOT NULL,
  PRIMARY KEY (`kd_nabung`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_nabung` */

insert  into `tb_nabung`(`kd_nabung`,`nominal_uang`,`tgl_nabung`,`id_user`,`status`) values 
('NBG0001',15000,'2021-01-18','USR0001','+'),
('NBG0002',15000,'2021-01-18','USR0001','+'),
('NBG0003',1000,'2021-01-18','USR0001','-'),
('NBG0004',25000,'2021-01-20','USR0001','+'),
('NBG0005',12000,'2021-01-22','USR0001','+'),
('NBG0006',14500,'2021-01-23','USR0001','+'),
('NBG0007',500,'2021-01-23','USR0001','-'),
('NBG0008',15000,'2021-01-29','USR0001','+');

/*Table structure for table `tb_totalnabung` */

DROP TABLE IF EXISTS `tb_totalnabung`;

CREATE TABLE `tb_totalnabung` (
  `id_totalnabung` varchar(7) NOT NULL,
  `total_nabung` int(10) NOT NULL DEFAULT 0,
  `tabungan_sebelumnya` int(10) NOT NULL DEFAULT 0,
  `total_pengeluaran` int(10) NOT NULL DEFAULT 0,
  `pengeluaran_sebelumnya` int(10) NOT NULL DEFAULT 0,
  `tgl_update` datetime NOT NULL DEFAULT current_timestamp(),
  `tgl_dibuat` datetime NOT NULL DEFAULT current_timestamp(),
  `id_user` varchar(7) NOT NULL,
  PRIMARY KEY (`id_totalnabung`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_totalnabung` */

insert  into `tb_totalnabung`(`id_totalnabung`,`total_nabung`,`tabungan_sebelumnya`,`total_pengeluaran`,`pengeluaran_sebelumnya`,`tgl_update`,`tgl_dibuat`,`id_user`) values 
('TNB0001',95000,80000,1500,500,'2021-01-29 08:48:39','2021-01-11 17:28:19','USR0001'),
('TNB0002',0,0,0,0,'2021-01-14 18:57:28','2021-01-14 06:47:35','USR0002');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id_user` varchar(7) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_depan` varchar(25) NOT NULL,
  `nama_belakang` varchar(120) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_user` */

insert  into `tb_user`(`id_user`,`username`,`password`,`nama_depan`,`nama_belakang`,`last_login`,`created_at`,`is_active`) values 
('USR0001','matoi07','$2y$10$IsZzCwa0.i3lQWHDx1Y9neF.On9heRIrFckG3mXUrDffG0aDijveu','Adhitya','Dwi Cahyana','2021-04-21 05:16:05','2021-01-06 03:33:53',1),
('USR0002','10116010','$2y$10$bMPIVGrExlo52XxzufAjn..BF95oT9ilQeRHXQ/BBHx1mFP/HR2xS','Adhitya','Dwi Cahyana','2021-01-14 18:47:40','2021-01-14 06:47:35',1);

/*Table structure for table `tb_wishlist` */

DROP TABLE IF EXISTS `tb_wishlist`;

CREATE TABLE `tb_wishlist` (
  `id_wishlist` varchar(7) NOT NULL,
  `nama_item` varchar(255) NOT NULL,
  `jml_item` int(4) NOT NULL,
  `nominal_wishlist` int(10) NOT NULL,
  `link_gambar` varchar(255) NOT NULL DEFAULT 'https://grafikaindah.com/images/produk/cetak/1551662819image-default.png',
  `jml_total` int(10) NOT NULL,
  `deskripsi_wishlist` longtext NOT NULL,
  `tgl_dibuat` datetime NOT NULL,
  `status` enum('diterima','ditolak','pending') NOT NULL DEFAULT 'pending',
  `id_user` varchar(7) NOT NULL,
  PRIMARY KEY (`id_wishlist`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_wishlist` */

insert  into `tb_wishlist`(`id_wishlist`,`nama_item`,`jml_item`,`nominal_wishlist`,`link_gambar`,`jml_total`,`deskripsi_wishlist`,`tgl_dibuat`,`status`,`id_user`) values 
('WSH0001','Xiaomi Redmi Note 8 4/64',1,2800000,'https://s1.bukalapak.com/img/65123615502/large/data.jpeg',2800000,'HP Xiaomi Redmi Note 8                                                                                                                                                                                                                                                                                        ','2021-01-14 21:47:01','pending','USR0001'),
('WSH0002','Xiaomi Redmi 7A 2/16',1,1299000,'https://www.begawei.com/wp-content/uploads/2019/05/xiaomi-redmi-7a-F.png',1299000,'HP xiaomi redmi 7a','2021-01-13 12:21:27','pending','USR0001');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
