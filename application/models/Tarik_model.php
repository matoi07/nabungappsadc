<?php defined('BASEPATH') OR exit('No direct script allowed');
date_default_timezone_set('Asia/Jakarta');
class tarik_model extends CI_Model
{
	public function rules()
	{
		return[
			[
				'field'		=> 'totalambil',
				'label'		=> '<i>Tabungan Diambil</i>',
				'rules'		=> 'required|numeric|less_than_equal_to['. $this->input->post('totaltabungan') .']',
			],
		];
	}
	public function save()
	{
		// Bagian Update total tabungan
		$id_user			= $this->input->post('id_user');
		$id_totalnabung		= $this->input->post('id_totalnabung');
		$totaltabungan		= $this->input->post('totaltabungan');
		$totalambil			= $this->input->post('totalambil');
		$total_pengeluaran	= $this->input->post('total_pengeluaran');
		$pengeluaransebelum	= $this->input->post('pengeluaran_sebelumnya');
		$pengeluarantotal	= $totalambil + $pengeluaransebelum;
		$hitungpengeluaran	= $totaltabungan - $totalambil;
		$data_pengeluaran	= array(
			'total_nabung'			=> $hitungpengeluaran,
			'tabungan_sebelumnya'	=> intval($totaltabungan),
			'total_pengeluaran'		=> $total_pengeluaran + $totalambil,
			'pengeluaran_sebelumnya'=> $totalambil,
			'tgl_update'			=> date('Y-m-d H:i:s'),
		);
		//var_dump($data_pengeluaran);
		//die();
		$log_id				= $this->input->post('log_id');
		$log_type			= $this->input->post('log_type');
		$kd_type			= $this->input->post('kd_type');
		$deskripsi			= $this->input->post('deskripsi');
		$data_log_pengeluaran	= array(
			'log_id'			=> $log_id,
			'log_type'			=> $log_type,
			'kd_aksi'			=> $id_totalnabung,
			'kd_type'			=> $kd_type,
			'nominal_uang'		=> $totalambil,
			'tgl_dibuat'		=> date('Y-m-d H:i:s'),
			'pesan'				=> $deskripsi,
			'id_user'			=> $id_user,
		);
		//var_dump($data_log_pengeluaran);
		//die();
		$kd_nabung 			= $this->input->post('kd_nabung');
		$data_utk_nabung	= array(
			'kd_nabung'		=> $kd_nabung,
			'nominal_uang'	=> $totalambil,
			'tgl_nabung'	=> date('Y-m-d'),
			'id_user'		=> $id_user,
			'status'		=> '-',
		);
		//var_dump($data_utk_nabung);
		//die();
		$this->db->where('id_totalnabung', $id_totalnabung);
		$this->db->where('id_user', $id_user);
		$this->db->update('tb_totalnabung', $data_pengeluaran);
		$this->db->insert('log_user', $data_log_pengeluaran);
		$this->db->insert('tb_nabung', $data_utk_nabung);
	}
}

?>