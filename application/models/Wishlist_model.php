<?php defined('BASEPATH') OR exit('No directory script allowed');

date_default_timezone_set('Asia/Jakarta');
class Wishlist_model extends CI_Model
{
	private $_table_wishlist 	= "tb_wishlist";
	private $id_wishlist;
	private $nama_wishlist;
	private $nominal_wishlist;
	private $deskripsi_wishlist;

	public function rules()
	{
		return[
				[
					'field'		=>		'nominal_item',
					'label'		=>		'Nominal Item',
					'rules'		=>		'required|numeric',
				]
		];
	}
	public function getAll()
	{
		$user_id	= $this->session->userdata('user_logged')->id_user;
		$db 		= $this->load->database('default', true);
		$query 		= $db->query(
			"
			SELECT 	*
			FROM  	tb_wishlist
			WHERE 	status = 'diterima'
			AND  	id_user = '".$user_id."'
			"
		);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	public function getById($id)
	{
		return $this->db->get_where($this->_table_wishlist, ["id_wishlist" => $id])->row();
	}
	public function getKodeWishlist()
	{
		$this->db->select('RIGHT(id_wishlist,3) as kodebaru', FALSE);
		$this->db->order_by('id_wishlist', 'DESC');
		$this->db->limit(1);
		$query 		= $this->db->get('tb_wishlist');
		if($query->num_rows()<>0){
			$data 		= $query->row();
			$kodebaru 	= intval($data->kodebaru) + 1;
		}else{
			$kodebaru 	= 1;
		}
		$kodemax 		= "WSH" . str_pad($kodebaru, 4, "0", STR_PAD_LEFT);
		$kodejadi  		= $kodemax;
		return $kodejadi;
	}
	public function save()
	{
		$id_wishlist 		= $this->input->post('kd_wishlist');
		$nama_item 			= $this->input->post('nama_wishlist');
		$nominal_item		= $this->input->post('nominal_item');
		$jml_item	 		= $this->input->post('jml_item');
		$total_hitung		= $nominal_item * $jml_item;
		$img_wishlist 		= $this->_uploadImageWishlist();
		$deskripsi 			= $this->input->post('deskripsi');
		$tgl_dibuat 		= date('Y-m-d H:i:s');
		$id_user			= $this->input->post('id_user');
		$data_wishlist 		= array(
			'id_wishlist'		=> $id_wishlist,
			'nama_item'			=> $nama_item,
			'jml_item'			=> $jml_item,
			'nominal_wishlist'	=> $nominal_item,
			'wishlist_img'		=> $img_wishlist,
			'jml_total'			=> $total_hitung,
			'deskripsi_wishlist'=> $deskripsi,
			'tgl_dibuat'		=> $tgl_dibuat,
			'id_user'			=> $id_user,
		);
		//var_dump($data_wishlist);
		//die();
		$log_id 			= $this->input->post('log_id');
		$log_type			= $this->input->post('log_type');
		$log_date 			= $this->input->post('log_date');
		$log_desc 			= $this->input->post('log_desc');
		$data_log 			= array(
			'log_id'			=> $log_id,
			'log_type'			=> $log_type,
			'kd_aksi'			=> $id_wishlist,
			'kd_type'			=> 'WISHLIST',
			'nominal_uang'		=> $total_hitung,
			'tgl_dibuat'		=> $tgl_dibuat,
			'pesan'				=> $log_desc,
			'id_user'			=> $id_user,
		);
		//var_dump($data_log);
		//die();
		$this->db->insert('tb_wishlist', $data_wishlist);
		$this->db->insert('log_user', $data_log);
	}
	public function update()
	{
		$id_wishlist		= $this->input->post('kd_wishlist');
		$nama_item 			= $this->input->post('nama_item');
		$nominal_item 		= $this->input->post('nominal_item');
		$jml_item			= $this->input->post('jml_item');
		$total_hitung		= $nominal_item * $jml_item;
		$deskripsi 			= $this->input->post('deskripsi');
		$tgl_dibuat			= date('Y-m-d H:i:s');
		$id_user			= $this->input->post('id_user');
		//$wishlist_img 		= $this->_uploadImageWishlist();
		if($wishlist_img 	= !NULL)
		{
			$wishlist_img 	= $this->_uploadImageWishlist();
		}else{
			$wishlist_img 	= $this->input->post('old_link_gambar');
		}
		$data_wishlist_upd	= array(
			'id_wishlist'		=> $id_wishlist,
			'nama_item'			=> $nama_item,
			'jml_item'			=> $jml_item,
			'nominal_wishlist'	=> $nominal_item,
			'wishlist_img'		=> $wishlist_img,
			'jml_total'			=> $total_hitung,
			'deskripsi_wishlist'=> $deskripsi,
			'tgl_dibuat'		=> $tgl_dibuat,
			'id_user'			=> $id_user,	
		);
		//var_dump($data_wishlist_upd);
		//die();
		$log_id				= $this->input->post('log_id');
		$log_type			= $this->input->post('log_type');
		$log_date			= $this->input->post('log_date');
		$log_desc			= $this->input->post('log_desc');
		$data_log_wishlist	= array(
			'log_id'			=> $log_id,
			'log_type'			=> $log_type,
			'kd_aksi'			=> $id_wishlist,
			'kd_type'			=> 'WISHLIST',
			'nominal_uang'		=> $total_hitung,
			'tgl_dibuat'		=> $log_date,
			'pesan'				=> $log_desc,
			'id_user'			=> $id_user,
		);
		//var_dump($data_log_wishlist);
		//die();
		$this->db->where('id_wishlist', $id_wishlist);
		$this->db->update($this->_table_wishlist, $data_wishlist_upd);
		$this->db->insert('log_user', $data_log_wishlist);
	}
	private function _uploadImageWishlist()
	{
		$kd_wishlist 			= $this->input->post('kd_wishlist');
		$date 					= date('d-m-Y');
		$config['upload_path']	= './dist/assets/upload/wishlist';
		$config['allowed_types']= 'gif|jpg|png';
		$config['file_name']	= 'IMG-'.$kd_wishlist.'-'.$date;
		$config['overwrite']	= true;
		$config['max_size']		= 1024;
		$this->load->library('upload', $config);

		if($this->upload->do_upload('image')){
			return $this->upload->data('file_name');
		}
		// print_r($this->upload->display_errors());

		return "default.jpg";
	}
	private function _deleteImage($id)
	{
		$wishlist 		= $this->getById($id);
		$image 			= $wishlist->wishlist_img;
		if($image != "default.jpg")
		{
			$filename 	= explode(".", $wishlist->wishlist_img)[0];
			return array_map('unlink', glob(FCPATH."dist/assets/upload/wishlist/$filename.*"));
		}
		//var_dump($image);
		//die();
	}
	public function delete($id)
	{
		$this->_deleteImage($id);
		$db 	= $this->load->database('default', true);
		$query 	= $db->query(
			"
			DELETE FROM ".$this->_table_wishlist."
			WHERE id_wishlist = '".$id."'
			"
		);
		return $query;
	}
	public function terima($id_wishlist)
	{
		$status 		= 'diterima';
		$tgl_update 	= date('Y-m-d H:i:s');
		$data_terima 	= array(
			'id_wishlist' 	=> $id_wishlist,
			'tgl_update'	=> $tgl_update,
			'status'		=> $status,
		);
		$this->db->where('id_wishlist', $id_wishlist);
		$this->db->update($this->_table_wishlist, $data_terima);
		//var_dump($data);
		//die();
	}
	public function tolak($id_wishlist)
	{
		//var_dump($id_wishlist);
		//die();
	}
}

?>