<?php defined('BASEPATH') OR exit('No direct script allowed');
date_default_timezone_set('Asia/Jakarta');
class Nabung_model extends CI_Model
{
	private $_table 		= "tb_nabung";
	private $_table_total	= "tb_totalnabung";
	private $kd_nabung;
	private $nominal_uang;
	private $tgl_nabung;

	public function rules()
	{
		return[
				[
					'field'		=>		'nominal_uang',
					'label'		=>		'Nominal Uang',
					'rules'		=>		'required|numeric',
				]
		];
	}

	public function getAll()
	{
		$user_id	= $this->session->userdata('user_logged')->id_user;
		$db 		= $this->load->database('default', true);
		$query 		= $db->query(
			"
			SELECT 	*
			FROM  	tb_nabung
			WHERE  	id_user = '".$user_id."'
			"
		);
		if($query->num_rows() > 0)
		{
			return $query;
		}
	}

	public function getById($id)
	{
		$this->db->where('kd_nabung', $id);
		return $this->db->get_where($this->_table)->row();
	}
	public function getKodeNabung()
	{
		$this->db->select('RIGHT(kd_nabung,3) as kodebaru', FALSE); // pilih kode
		$this->db->order_by('kd_nabung', 'DESC');					// urut dari terakhir ke paling awal
		$this->db->limit(1);										// pilih 1 paling terakhir
		$query 		= $this->db->get('tb_nabung');					// ambil table
		if($query->num_rows()<>0){									// cek apakah table != 0
			$data 		= $query->row();							// munculin query
			$kodebaru 	= intval($data->kodebaru) + 1;				// pembuatan kode baru + 1
		}else{			
			$kodebaru 	= 1;										// kalo gaada kodenya, maka kasih 1
		}
		$kodemax 		= "NBG" . str_pad($kodebaru, 4, "0", STR_PAD_LEFT); // hasilnya jadi NBG0001 (1 terakhir = kode baru)
		$kodejadi  		= $kodemax;											// ubah nama doang
		return $kodejadi;											// lempar deh
	}
	public function save()
	{
		$kd_nabung		= $this->input->post('kd_nabung');
		$nominal_uang	= $this->input->post('nominal_uang');
		$tgl_nabung		= $this->input->post('tgl_nabung');
		$id_user		= $this->input->post('id_user');
		$data_nabung	= array(
			'kd_nabung'		=> $kd_nabung,
			'nominal_uang'	=> $nominal_uang,
			'tgl_nabung'	=> $tgl_nabung,
			'id_user'		=> $id_user,
			'status'		=> '+',
		);
		//var_dump($data_nabung);
		//die();
		$log_id			= $this->input->post('log_id');
		$log_desc 		= $this->input->post('log_desc');
		$log_date		= $this->input->post('log_date');
		$log_type 		= $this->input->post('log_type');
		$data_log_add	= array(
			'log_id'		=> $log_id,
			'log_type'		=> $log_type,
			'kd_aksi'		=> $kd_nabung,
			'kd_type'		=> 'NABUNG',
			'nominal_uang'	=> $nominal_uang,
			'tgl_dibuat'	=> $log_date,
			'pesan'			=> $log_desc,
			'id_user'		=> $id_user,
		);
		//var_dump($data_log);
		//die();
		$totalnabung 		= $this->input->post('total_nabung');
		$tabungan_sebelumnya= $this->input->post('tabungansebelumnya');
		$id_totalnabung		= $this->input->post('id_totalnabung');
		$data_nabung_add= array(
			'total_nabung'			=> $totalnabung + $nominal_uang,
			'tabungan_sebelumnya'	=> $totalnabung,
			'tgl_update'			=> date('Y-m-d H:i:s'),
		);
		//var_dump($data_nabung_add);
		//die();
		$this->db->insert('tb_nabung', $data_nabung);
		$this->db->insert('log_user', $data_log_add);
		$this->db->where('id_totalnabung', $id_totalnabung);
		$this->db->where('id_user', $id_user);
		$this->db->update('tb_totalnabung', $data_nabung_add);
	}
	public function update()
	{
		$kd_nabung		= $this->input->post('kd_nabung');
		$nominal_uang	= $this->input->post('nominal_uang');
		$tgl_nabung		= $this->input->post('tgl_nabung');
		$id_user		= $this->input->post('id_user');
		$data_update_nabung	= array(
			'kd_nabung'		=> $kd_nabung,
			'nominal_uang'	=> $nominal_uang,
			'tgl_nabung'	=> $tgl_nabung,
			'id_user'		=> $id_user,
			'status'		=> '+',
		);
		//var_dump($data_update_nabung);
		//die();
		$log_id			= $this->input->post('log_id');
		$log_desc 		= $this->input->post('log_desc');
		$log_date		= $this->input->post('log_date');
		$log_type 		= $this->input->post('log_type');
		$data_log_update= array(
			'log_id'		=> $log_id,
			'log_type'		=> $log_type,
			'kd_aksi'		=> $kd_nabung,
			'kd_type'		=> 'NABUNG',
			'nominal_uang'	=> $nominal_uang,
			'tgl_dibuat'	=> $log_date,
			'pesan'			=> $log_desc,
			'id_user'		=> $id_user,
		);
		$id_totalnabung		= $this->input->post('id_totalnabung');
		$total_nabung		= $this->input->post('total_tabung');
		$nominal_uang_old	= $this->input->post('nominal_uang_old');
		$total_tabung_lama 	= abs($nominal_uang_old - $total_nabung);
		$total_tabung_baru  = $nominal_uang + $total_tabung_lama;
		$data_tb_update	= array(
			'total_nabung'			=> $total_tabung_baru,
			'tabungan_sebelumnya' 	=> $total_nabung,
		);
		//var_dump($data_tb_update);
		//die();
		$this->db->where('kd_nabung', $kd_nabung);
		$this->db->update($this->_table, $data_update_nabung);
		$this->db->insert('log_user', $data_log_update);
		$this->db->where('id_totalnabung', $id_totalnabung);
		$this->db->where('id_user', $id_user);
		$this->db->update('tb_totalnabung', $data_tb_update);
	}
	public function delete($id)
	{
		return $this->db->delete($this->_table, array("kd_nabung" => $id));
	}
}

?>