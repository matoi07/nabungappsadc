<?php defined('BASEPATH') OR exit('No directory script allowed');
date_default_timezone_set('Asia/Jakarta');
class overview_model extends CI_Model
{
	public function getAllWishlist()
	{
		$id_user	= $this->session->userdata('user_logged')->id_user;
		return $this->db->get_where('tb_wishlist', ["id_user" => $id_user])->result();
	}
}

?>