<?php 
date_default_timezone_set('Asia/Jakarta');
class User_model extends CI_Model
{
	private $_table		= "tb_user";
	public function __construct()
	{
		parent:: __construct();
		$this->load->library('form_validation');
	}
	public function rules()
	{
		return[
			[
				'field' 	=> 	'username',
				'label' 	=> 	'Username',
				'rules' 	=>	'required',
			],
			[
				'field'		=> 'password',
				'label'		=> 'Password',
				'rules'		=> 'required',
			],
		];
	}
	public function rules_register()
	{
		return[
			[
				'field' 	=> 	'username',
				'label' 	=> 	'Username',
				'rules' 	=>	'required',
			],
			[
				'field'		=> 'password',
				'label'		=> 'Password',
				'rules'		=> 'required',
			],
			[
				'field'		=> 'namadepan',
				'label'		=> 'Nama Depan',
				'rules'		=> 'required',
			],
			[
				'field'		=> 'namabelakang',
				'label'		=> 'Nama Belakang',
				'rules'		=> 'required',
			],
			[
				'field'		=> 'password_match',
				'label'		=> 'Password Match',
				'rules'		=> 'matches[password]',
			]
		];
	}
	public function rules_savePassword()
	{
		return[
			[
				'field'		=> 'username',
				'label'		=> 'Username',
				'rules'		=> 'required',
			],
		];
	}
	public function doLogin()
	{
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$this->db->where('username', $username);
		$user 		= $this->db->get($this->_table)->row();

		// Jika user terdaftar
		if($user)
		{
			$isPasswordTrue	= password_verify($password, $user->password);
			if($isPasswordTrue)
			{
				$this->session->set_userdata(['user_logged' => $user]);
				$this->_updateLastLogin($user->id_user);
				return true;
			}
		}
		//Login Gagal
		return false;
	}
	public function isNotLogin()
	{
		return $this->session->userdata('user_logged') === null;
	}
	private function _updateLastLogin($user_id)
	{
		$id_user 	= $user_id;
		$last_login = date('Y-m-d H:i:s');
		$data 	= array(
			'id_user' 	=> $id_user,
			'last_login'=> $last_login,
		);
		$this->db->where('id_user', $id_user);
		$this->db->update($this->_table, $data);
	}
	public function RegisterUser()
	{
		$id_user		= $this->input->post('id_user');
		$username 		= $this->input->post('username');
		$password 		= $this->input->post('password');
		$nama_depan		= $this->input->post('namadepan');
		$nama_belakang	= $this->input->post('namabelakang');
		$data_registrasi= array(
			'id_user'		=> $id_user,
			'username'		=> $username,
			'password'		=> password_hash($password, PASSWORD_DEFAULT),
			'nama_depan'	=> $nama_depan,
			'nama_belakang'	=> $nama_belakang,
			'created_at'	=> date('Y-m-d H:i:s'),
			'is_active'		=> '1',
		);
		//var_dump($data_registrasi);
		//die();
		$id_totalnabung				= $this->input->post('id_totaltabung');
		$data_tambah_totalnabung	= array(
			'id_totalnabung'		=> $id_totalnabung,
			'tgl_update'			=> date('Y-m-d H:i:s'),
			'tgl_dibuat'			=> date('Y-m-d H:i:s'),
			'id_user'				=> $id_user,
		);
		//var_dump($data_tambah_totalnabung);
		//die();
		$log_id		= $this->input->post('log_id');
		$data_log 	= array(
			'log_id'		=> $log_id,
			'log_type'		=> 'TAMBAH',
			'kd_aksi'		=> $id_user,
			'kd_type'		=> 'AKUN',
			'nominal_uang'	=> 0,
			'tgl_dibuat'	=> date('Y-m-d H:i:s'),
			'pesan'			=> 'Akun telah ditambahkan',
			'id_user'		=> $id_user,
		);
		//var_dump($data_log);
		//die();
		$this->db->insert('tb_user', $data_registrasi);
		$this->db->insert('tb_totalnabung', $data_tambah_totalnabung);
		$this->db->insert('log_user', $data_log);
	}
	public function getDataUser()
	{
		$user_id	= $this->session->userdata('user_logged')->id_user;
		$this->db->where('id_user', $user_id);
		return $this->db->get('tb_user')->row();
	}
	public function savePassword()
	{
		$id_user		= $this->session->userdata('user_logged')->id_user;
		$username 		= $this->input->post('username');
		$password_lama	= $this->input->post('password_lama');
		$password_baru	= $this->input->post('password_baru');
		$retype_password= $this->input->post('retype_password_baru');
		if(empty($password_baru)){
			$password 	= $password_lama;
		}else{
			$password 	= password_hash($password_baru, PASSWORD_DEFAULT);
		}
		$data_save_password = array(
			'username'		=> $username,
			'password'		=> $password,
		);
		//var_dump($data_save_password);
		//die();
		$log_id				= $this->input->post('log_id');
		$log_type			= $this->input->post('log_type');
		$kd_aksi			= $id_user;
		$kd_type			= $this->input->post('kd_type');
		$nominal_uang		= '-';
		$tgl_dibuat			= date('Y-m-d H:i:s');
		$pesan				= $this->input->post('pesan_log');
		$data_log_profile	= array(
			'log_id'		=> $log_id,
			'log_type'		=> $log_type,
			'kd_aksi'		=> $kd_aksi,
			'kd_type'		=> $kd_type,
			'nominal_uang'	=> $nominal_uang,
			'tgl_dibuat'	=> $tgl_dibuat,
			'pesan'			=> $pesan,
			'id_user'		=> $id_user,
		);
		//var_dump($data_log_profile);
		//die();
		$this->db->where('id_user', $id_user);
		$this->db->update('tb_user', $data_save_password);
		$this->db->insert('log_user', $data_log_profile);
	}
	public function getAllLogUser()
	{
		$user_id 	= $this->session->userdata('user_logged')->id_user;
		$db 		= $this->load->database('default', true);
		$query 		= $db->query(
			"
				SELECT *
				FROM log_user
				WHERE id_user = '".$user_id."'
				ORDER BY tgl_dibuat DESC
			"
		);
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
	}
}

?>