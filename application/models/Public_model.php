<?php defined('BASEPATH') OR exit('No directory allowed');
date_default_timezone_set('Asia/Jakarta');
class public_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$user_id	= $this->session->userdata('user_logged')->id_user;
	}
	public function getKodeLog()
	{
		$this->db->select('RIGHT(log_id,3) as kodelogbaru', FALSE);
		$this->db->order_by('log_id', 'DESC');
		$this->db->limit(1);
		$query		= $this->db->get('log_user');
		if($query->num_rows()<>0){
			$data 			= $query->row();
			$kodelogbaru 	= intval($data->kodelogbaru) + 1;
		}else{
			$kodelogbaru 	= 1;
		}
		$kodemax 			= "LOG" . str_pad($kodelogbaru, 4, "0", STR_PAD_LEFT);
		$kodejadi 			= $kodemax;
		return $kodejadi;
	}
	public function getKodeTotalTabung()
	{
		$this->db->select('RIGHT(id_totalnabung,3) as kodetotalnabung', FALSE);
		$this->db->order_by('id_totalnabung','DESC');
		$this->db->limit(1);
		$query 		= $this->db->get('tb_totalnabung');
		if($query->num_rows()<>0){
			$data 				= $query->row();
			$kodetotalnabung	= intval($data->kodetotalnabung) + 1;
		}else{
			$kodetotalnabung	= 1;
		}
		$kodemax				= "TNB" . str_pad($kodetotalnabung, 4, "0", STR_PAD_LEFT);
		$kodejadi 				= $kodemax;
		return $kodejadi;
	}
	public function getKodeNabung()
	{
		$this->db->select('RIGHT(kd_nabung,3) as kodebaru', FALSE);
		$this->db->order_by('kd_nabung', 'DESC');
		$this->db->limit(1);
		$query 		= $this->db->get('tb_nabung');
		if($query->num_rows()<>0){
			$data 		= $query->row();
			$kodebaru 	= intval($data->kodebaru) + 1;
		}else{
			$kodebaru 	= 1;
		}
		$kodemax 		= "NBG" . str_pad($kodebaru, 4, "0", STR_PAD_LEFT);
		$kodejadi  		= $kodemax;
		return $kodejadi;
	}
	public function getKodeUser()
	{
		$this->db->select('RIGHT(id_user,3) as kodebaru', FALSE);
		$this->db->order_by('id_user', 'DESC');
		$this->db->limit(1);
		$query 		= $this->db->get('tb_user');
		if($query->num_rows()<>0){
			$data 		= $query->row();
			$kodebaru 	= intval($data->kodebaru) + 1;
		}else{
			$kodebaru 	= 1;
		}
		$kodemax 		= "USR" . str_pad($kodebaru, 4, "0", STR_PAD_LEFT);
		$kodejadi  		= $kodemax;
		return $kodejadi;
	}
	public function getTotalNabung()
	{
		$user_id	= $this->session->userdata('user_logged')->id_user;
		$db 		= $this->load->database('default', true);
		$query 		= $this->db->query(
			"
			SELECT 	*
			FROM  	tb_totalnabung
			WHERE  	id_user = '".$user_id."'
			"
		);
		if($query->num_rows() > 0){
			return $query->row();
		}
		//return $this->db->get_where('tb_totalnabung', ["id_user" => $user_id])->row();
	}
	public function getTotalNabungHarian()
	{
		$user_id 		= $this->session->userdata('user_logged')->id_user;
		$datenow		= date("Y-m-d");
		$status_nabung	= '+';
		$this->db->select_sum('nominal_uang');
		return $this->db->get_where('tb_nabung', ["id_user" 	=> $user_id,
												  "tgl_nabung"	=> $datenow,
												  "status"		=> $status_nabung,
												 ]
									)->row();
	}
	public function getDataUser()
	{
		$user_id	= $this->session->userdata('user_logged')->id_user;
		return $this->db->get_where('tb_user', ["id_user" => $user_id])->row();
	}
	public function getAllTabungan()
	{
		$user_id	= $this->session->userdata('user_logged')->id_user;
		$this->db->where('id_user', $user_id);
		return $this->db->get('tb_nabung')->result();
	}
	public function getTambahTabunganUser()
	{
		$user_id	= $this->session->userdata('user_logged')->id_user;
		$status 	= '+';
		$db 		= $this->load->database('default', true);
		$query 		= $db->query(
			"
			SELECT 		MONTHNAME(tgl_nabung) as bulan,
						SUM(nominal_uang) as total
			FROM  		tb_nabung
			WHERE  		id_user = '".$user_id."'
			AND 		status 	= '".$status."'
			GROUP BY 	bulan
			"
		);
		if($query->num_rows() > 0){
			return $query->result();
		}
	}
	// START GET ALL MENU USER

	// END GET ALL MENU USER
}

?>