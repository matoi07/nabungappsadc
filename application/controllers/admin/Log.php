<?php 

class log extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('user_model','user');
	}
	public function index()
	{
		$user 				= $this->user;
		// $role 				= $this->session->userdata('user_logged')->id_role;
		// var_dump($this->session->userdata('user_logged'));die();
		$data["LogUser"]	= $user->getAllLogUser();
		$data 				= array(
			'LogUser'		=> $user->getAllLogUser(),
			'username'		=> $this->session->userdata('user_logged')->username,
			'db'			=> $this->load->database('default', true),
			'title'			=> "Home",
			// 'role'				=> $role,
		);
		$this->load->view('admin/log/list_log_user', $data);
	}
	
	public function getLogUser()
	{
		$draw 	= intval($this->input->get("draw"));
		$start 	= intval($this->input->get("start"));
		$length = intval($this->input->get("length"));

		$log_list 	= $this->user->getAllLogUser();
		$data 		= array();
		$no 		= $this->input->post('start');
		foreach($log_list as $tarik):
			$no++;
			$row 	= array();
			$row[] 	= $no;
			$row[] 	= "<td style='color: green'>".$tarik['log_type']."</td>";
			$row[] 	= $tarik['tgl_dibuat'];
			$row[] 	= $tarik['pesan'];
			$data[]	= $row;
		endforeach;
		$output 	= array(
			"draw"	=> $draw,
			"data"	=> $data,
		);
		echo json_encode($output);
		exit();
	}
	/*
	public function getLogUser()
	{
		$data = $this->user_model->getAllLogUser();
		echo json_encode($data);
		exit();
	}
	*/
}

?>