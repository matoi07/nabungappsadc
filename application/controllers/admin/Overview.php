<?php
class Overview extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("user_model");
		$this->load->model("public_model");
		$this->load->model("overview_model");
		$this->load->model("wishlist_model");
		if($this->user_model->isNotLogin()){
			redirect(site_url('admin/login'));
		}
	}
	public function index()
	{
		$public 	= $this->public_model;
		$overview 	= $this->overview_model;
		$role  		= $this->session->userdata('user_logged')->id_role;
		$data 		= array(
			'totalnabung'		=> $public->getTotalNabung(),
			'totalharian'		=> $public->getTotalNabungHarian(),
			'wishlist'			=> $overview->getAllWishlist(),
			'username'			=> $this->session->userdata('user_logged')->username,
			'db'				=> $this->load->database('default', true),
			'title'				=> "Home",
			'role'				=> $role,
		);
		//var_dump($data['menu']);die();
		// $data["totalnabung"]	= $public->getTotalNabung();
		// $data["totalharian"]	= $public->getTotalNabungHarian();
		// $data["wishlist"]		= $overview->getAllWishlist();
		// $data["username"]		= $this->session->userdata('user_logged')->username;
		// $data["menu"]			= $public->getAllMenu($role);
		$this->load->view('admin/index', $data);
	}
	public function profile()
	{
		$user 					= $this->user_model;
		$public 				= $this->public_model;
		$validation 			= $this->form_validation;
		$role  					= $this->session->userdata('user_logged')->id_role;
		$data["username"]		= $this->session->userdata('user_logged')->username;
		$data["db"]				= $this->load->database('default', true);
		$data["role"]			= $this->session->userdata('user_logged')->id_role;
		//$data["menu"]			= $public->getAllMenu($role);
		//$data["group"]			= $public->getGroupMenu($role);
		$validation->set_rules($user->rules_savePassword());
		if($validation->run())
		{
			$user->savePassword();
			$this->session->set_flashdata('success','Password Berhasil diubah');
			redirect(site_url('admin/profile'));
		}
		$data["user"]			= $user->getDataUser();
		$data["chart"]			= $public->getTambahTabunganUser();
		$data["kodeLog"]		= $public->getKodeLog();
		$this->load->view('admin/profile', $data);
		// cek jika user belum login dan ingin masuk kedalam halaman profile
	}
	public function LogUser()
	{
		$user 				= $this->user_model;
		$data["LogUser"]	= $user->getAllLogUser();
		$this->load->view('admin/log/list_log_user', $data);
	}
}
?>