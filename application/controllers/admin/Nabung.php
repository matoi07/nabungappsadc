<?php defined('BASEPATH') OR exit('No directory script access allowed');
	class Nabung extends CI_Controller{
		public function __construct()
		{
			parent::__construct();
			$this->load->model("public_model");
			$this->load->model("nabung_model");
			$this->load->model("user_model");
			$this->load->library('form_validation');
			if($this->user_model->isNotLogin())
			{
				redirect(site_url('admin/user'));
			}
		}
		public function index()
		{
			$data["nabung"] = $this->nabung_model->getAll();
			$this->load->view('admin/nabung/list_Tabungan', $data);
		}
		public function add()
		{
			$nabung 		= $this->nabung_model;
			$public 		= $this->public_model;
			$validation 	= $this->form_validation;
			$validation->set_rules($nabung->rules());
			if($validation->run()){
				$nabung->save();
				$this->session->set_flashdata('success','Tabungan berhasil ditambah');
				redirect(site_url('admin/nabung'));
			}
			$data['kodeunik']	= $nabung->getKodeNabung();
			$data['kodeLog'] 	= $public->getKodeLog();
			$data['totaltabungan'] = $public->getTotalNabung();
			$this->load->view('admin/nabung/Mulai_Nabung', $data);
		}
		public function edit($id = null)
		{
			if(!isset($id)) redirect('admin/nabung/');

			$nabung 		= $this->nabung_model;
			$public 		= $this->public_model;
			$validation 	= $this->form_validation;
			$validation->set_rules($nabung->rules());

			if($validation->run()){
				$nabung->update();
				$this->session->set_flashdata('success','Tabungan Berhasil di Perbarui');
				redirect(site_url('admin/nabung/'));
			}
			$data["totaltabungan"]	= $public->getTotalNabung();
			$data["nabung"] 		= $nabung->getById($id);
			$data["kodeLog"]		= $public->getKodeLog();
			if(!$data["nabung"]) show_404();

			$this->load->view("admin/nabung/Edit_Tabungan", $data);
		}
		public function view($id = null)
		{
			// Jika $id kosong, maka lempar ke halaman nabung/List
			if(!isset($id)) redirect('admin/nabung/');

			$public 		= $this->public_model;
			$nabung 		= $this->nabung_model;
			$data["nabung"]	= $nabung->getById($id);
			$data["userdata"]	= $public->getDataUser();

			// jika isian dari $data kosong, maka tampilkan 404
			if(!$data["nabung"]) show_404();

			$this->load->view("admin/nabung/Lihat_Tabungan", $data);
		}
		public function getAllNabung()
		{
			$draw 	= intval($this->input->get("draw"));
			$start 	= intval($this->input->get("start"));
			$length = intval($this->input->get("length"));

			$ambil 	= $this->nabung_model->getAll();
			$data 	= array();
			$no 	= $start;
				foreach($ambil as $nabung){
					$no++;
					$row 	= array();
					$row[] 	= $nabung['kd_nabung'];
					$row[] 	= $nabung['nominal_uang'];
					$row[] 	= $nabung['tgl_nabung'];
					$row[] 	= "<a href='#' class='btn btn-primary'>Change</a>";
					$data[] = $row;
				}
			$output = array(
				"draw"	=> $draw,
				"data"	=> $data
			);
			echo json_encode($output);
			exit();
		}
	}
?>