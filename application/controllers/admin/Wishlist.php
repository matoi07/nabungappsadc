<?php defined('BASEPATH') OR exit('No directory script access allowed');
	class Wishlist extends CI_Controller{
		public function __construct()
		{
			parent::__construct();
			$this->load->model("public_model");
			$this->load->model("wishlist_model");
			$this->load->model("user_model");
			$this->load->library('form_validation');
			if($this->user_model->isNotLogin())
			{
				redirect(site_url('admin/user'));
			}
		}
		public function index()
		{
			$data["wishlist"] = $this->wishlist_model->getAll();
			$this->load->view('admin/wishlist/List_Wishlist', $data);
		}
		public function add()
		{
			$wishlist 		= $this->wishlist_model;
			$public 		= $this->public_model;
			$validation 	= $this->form_validation;
			$validation->set_rules($wishlist->rules());
			if($validation->run()){
				$wishlist->save();
				$this->session->set_flashdata('success','Wishlist berhasil ditambah');
				redirect(site_url('admin/wishlist/'));
			}
			$data['kodeunik']	= $wishlist->getKodeWishlist();
			$data['kodeLog'] 	= $public->getKodeLog();
			$this->load->view('admin/wishlist/Buat_Wishlist', $data);
		}
		public function edit($id = null)
		{
			if(!isset($id)) redirect('admin/wishlist/');
			$wishlist 	= $this->wishlist_model;
			$public 	= $this->public_model;
			$validation = $this->form_validation;
			$validation->set_rules($wishlist->rules());
			if($validation->run()){
				$wishlist->update();
				$this->session->set_flashdata('success','Wishlist berhasil diubah');
				redirect(site_url('admin/wishlist/'));
			}
			$data["wishlist"]	= $wishlist->getById($id);
			$data["kodeLog"]	= $public->getKodeLog();
			if(!$data["wishlist"])
			{
				show_404();
			}
			$this->load->view('admin/wishlist/Edit_Wishlist', $data);
		}
		public function view($id = null)
		{
			if(!isset($id)) redirect('admin/wishlist/');
			$wishlist 		= $this->wishlist_model;
			$public 		= $this->public_model;
			$data["wishlist"]	= $wishlist->getById($id);
			$data["kodeLog"]	= $public->getKodeLog();
			if(!$data["wishlist"])
			{
				show_404();
			}
			$this->load->view('admin/wishlist/Lihat_Wishlist', $data);
		}
		public function delete($id = NULL)
		{
			if(!isset($id)){
				show_404();
			}
			if($this->wishlist_model->delete($id)){
				$this->session->set_flashdata('deleteSuccess','Wishlist berhasil dihapus');
				redirect(site_url('admin/wishlist','refresh'));
			}
		}
		public function action()
		{
			$terima 		= $this->input->post('terima');
			$tolak 			= $this->input->post('tolak');
			$id_wishlist 	= $this->input->post('id_wishlist');
			if($id_wishlist != NULL){
				if($terima){
					if($this->wishlist_model->terima($id_wishlist)){
						$this->session->set_flashdata('TerimaSuccess', 'Wishlist berhasil diterima');
						redirect(site_url('admin/wishlist','refresh'));
					}
				}else{
					if($this->wishlist_model->tolak($id_wishlist)){
						$this->session->set_flashdata('TolakSuccess', 'Wishlist telah ditolak');
						redirect(site_url('admin/wishlist','refresh'));
					}
				}
			}
		}
	}
?>