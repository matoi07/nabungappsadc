<?php

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("user_model");
		$this->load->model("public_model");
		$this->load->library('form_validation');
	}
	public function index()
	{
		redirect(site_url('admin/'));
	}
	public function register()
	{
		$public 		= $this->public_model;
		$user 			= $this->user_model;
		$validation 	= $this->form_validation;
		$validation->set_rules($user->rules_register());
		if($validation->run())
		{
			$user->RegisterUser();
			$this->session->set_flashdata('success','User berhasil ditambahkan');
			redirect(site_url('admin/'));
		}
		$data["kodeLog"]	= $public->getKodeLog();
		$data["kodeTNB"]	= $public->getKodeTotalTabung();
		$data["kodeUser"]	= $public->getKodeUser();
		$this->load->view('admin/register_page', $data);
	}
}

?>