<?php defined('BASEPATH') OR exit('No directory script access allowed');

	class tarik extends CI_Controller{
		public function __construct()
		{
			parent:: __construct();
			$this->load->model('public_model');
			$this->load->model('user_model');
			$this->load->model('tarik_model');
			$this->load->library('form_validation');
			if($this->user_model->isNotLogin())
			{
				redirect(site_url('admin/user'));
			}
		}
		public function index()
		{
			$public 				= $this->public_model;
			$tarik 					= $this->tarik_model;
			$data["kodeLog"]		= $public->getKodeLog();
			$data["totalnabung"]	= $public->getTotalNabung();
			$data["kodeNabung"]		= $public->getKodeNabung();
			$validation 			= $this->form_validation;
			$validation->set_rules($tarik->rules());
			if($validation->run())
			{
				$tarik->save();
				$this->session->set_flashdata('success','Tabungan berhasil diambil');
				redirect(site_url('admin/tarik'));
			}
			$this->load->view('admin/tarik/mulai_tarik', $data);
		}
	}

?>