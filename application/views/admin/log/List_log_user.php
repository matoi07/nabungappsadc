<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('admin/_partials/head.php'); ?>
</head>

<body id="page-top">
	<?php $this->load->view('admin/_partials/header.php'); ?>
	<div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <?php $this->load->view('admin/_partials/navigation_bar.php'); ?>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                    <h1 class="mt-4">Log</h1>
                    <!-- BREADCRUMBS AREA -->
                    <?php 
                    $this->load->view('admin/_partials/breadcrumb.php'); 
                    if($this->session->flashdata('success')):
                    ?>
                    <div class='alert alert-success' role='alert'>
                        <?php echo $this->session->flashdata('success'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class='fas fa-times'></i>
                        </button>
                    </div>
                    <?php
                    endif;
                    ?>
                    <div class='card mb-4'>
                            <div class='card-header container-fluid'>
                                <i class='fas fa-arrow-left'></i>
                                <a href='<?php echo base_url('admin/') ?>'>Kembali</a>
                            </div>
                            <div class='card-body'>
                            	<div class='table-responsive'>
                            		<table class='table table-hover' id='TableLog' width="100%" cellspacing="0">
                            			<thead>
                            				<tr>
                                                <th width="5%">No. </th>
                            					<th width="15%">Log Aksi</th>
                                                <th width="20%">Tgl. Dibuat</th>
                                                <th>Deskripsi</th>
                            				</tr>
                            			</thead>
                            			<tbody>
                            			</tbody>
                            		</table>
                            	</div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
</body>
<script type="text/javascript">
    $(document).ready(function(){
        $("#TableLog").DataTable({
            ajax:{
                url: '<?php echo site_url("admin/Log/getLogUser"); ?>',
                type: 'GET'
            },
        });
    });
</script>
</html>