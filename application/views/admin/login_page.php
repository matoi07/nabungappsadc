<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('admin/_partials/head.php'); ?>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">NabungApps Login</h3></div>
                                    <div class="card-body">
                                        <form method='post' action='<?= site_url('admin/login') ?>'>
                                            <div class="form-group">
                                                <label class="small mb-1" for='username'>Username</label>
                                                <input class="form-control form-control-sm <?php echo form_error('username') ? 'is-invalid':'' ?>" name='username' type='text' placeholder="Masukkan Username" autofocus />
                                                <div class='invalid-feedback'>
                                                    <?php echo form_error('username'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for='password'>Password</label>
                                                <input class="form-control form-control-sm <?php echo form_error('password') ? 'is-invalid':'' ?>" name='password' type='password' placeholder="Masukkan Password" />
                                                <div class="invalid-feedback">
                                                    <?php echo form_error('password'); ?>
                                                </div>
                                            </div>
                                            <button type='submit' class='form-control btn btn-primary'><i class='fas fa-sign-in-alt'></i> Login</button>
                                            <!--<input type="text" name="" value='<?php echo $this->session->userdata('user_logged')->id_user; ?>'>-->
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="<?php echo site_url('admin/user/register') ?>">Belum punya akun? Registrasi Sekarang!</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
    </body>
</html>
