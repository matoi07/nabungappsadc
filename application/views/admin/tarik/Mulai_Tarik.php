<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('admin/_partials/head.php'); ?>
</head>

<body id="page-top">
	<?php $this->load->view('admin/_partials/header.php'); ?>
	<div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <?php $this->load->view('admin/_partials/navigation_bar.php'); ?>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                    	<h1 class="mt-4">Nabung</h1>
                    	<!-- BREADCRUMBS AREA -->
                    	<?php 
                    		$this->load->view('admin/_partials/breadcrumb.php'); 
                    		if($this->session->flashdata('success')):
                    	?>
                    	<div class='alert alert-success' role='alert'>
                        	<?php echo $this->session->flashdata('success'); ?>
                        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            	<i class='fas fa-times'></i>
                        	</button>
                    	</div>
                    	<?php
                    		endif;
                    	?>
                    	<div class='card mb-4'>
                        	<div class='card-header container-fluid'>
								<i class='fas fa-arrow-left'></i>
								<a href='<?php echo base_url('admin/') ?>'>Kembali</a>
                            </div>
                            <div class='card-body'>
                            	<form method='post' action=''>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for='totaltabungan'>Total Tabungan</label>
                                                <input class='form-control <?php echo form_error('totaltabungan') ? 'is-invalid':'' ?>' type='text' value='<?php echo $totalnabung->total_nabung; ?>' name='totaltabungan' readonly>
                                                <div class='invalid-feedback'>
                                                    <?php echo form_error('totaltabungan') ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for='totalambil'>Tabungan diambil</label>
                                                <input class='form-control <?php echo form_error('totalambil') ? 'is-invalid':'' ?>' type='number' min='0' placeholder='Masukkan Jumlah yang akan diambil' name='totalambil' autofocus>
                                                <div class="invalid-feedback">
                                                    <?php echo form_error('totalambil'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type='hidden' name='pengeluaran_sebelumnya' value='<?php echo $totalnabung->pengeluaran_sebelumnya ?>'>
                                    <input type='hidden' name='total_pengeluaran' value='<?php echo $totalnabung->total_pengeluaran; ?>'>
                                    <input type='hidden' name='id_totalnabung' value='<?php echo $totalnabung->id_totalnabung; ?>'>
                                    <input type='hidden' name='kd_nabung' value='<?php echo $kodeNabung; ?>'>
                                    <input type='hidden' name='log_id' value='<?php echo $kodeLog; ?>'>
                                    <input type='hidden' name='log_type' value='KURANG'>
                                    <input type='hidden' name='kd_type' value='AMBIL'>
                                    <input type='hidden' name='deskripsi' value='Tabungan telah diambil'>
                                    <input type='hidden' name='id_user' value='<?php echo $totalnabung->id_user; ?>'>
                                    <button type='submit' class='btn btn-primary'><i class='fas fa-save'></i> Ambil</button>
                                    <a href='<?php echo base_url('admin/') ?>' class='btn btn-danger'><i class='fas fa-ban'></i> Tidak Jadi</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
</body>

</html>