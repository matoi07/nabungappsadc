<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('admin/_partials/head.php'); ?>
</head>

<body id="page-top">
    <?php $this->load->view('admin/_partials/header.php'); ?>
    <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <?php $this->load->view('admin/_partials/navigation_bar.php'); ?>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                    <h1 class="mt-4">Profile</h1>
                    <!-- BREADCRUMBS AREA -->
                    <?php 
                    $this->load->view('admin/_partials/breadcrumb.php'); 
                    if($this->session->flashdata('success')):
                    ?>
                    <div class='alert alert-success' role='alert'>
                        <?php echo $this->session->flashdata('success'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class='fas fa-times'></i>
                        </button>
                    </div>
                    <?php
                    endif;
                    ?>
                    <div class="row">
                    	<div class="col-md-6 h-100">
                    		<div class="card">
                    			<div class="card-header">
                    				<i class="fas fa-user"></i> Profilku
                    			</div>
                    			<div class="card-body">
                    				<form method='post' action=''>
                    				<div class="form-row">
                    					<div class="col-md-6">
                    						<div class="form-group">
                    							<label for='nama_depan' class='small'>Nama Depan</label>
                    							<input type='text' class='form-control-sm form-control' value='<?php echo $user->nama_depan; ?>' name='nama_depan' readonly>
                    						</div>
                    					</div>
                    					<div class="col-md-6">
                    						<div class="form-group">
                    							<label class="small" for='nama_belakang'>Nama Belakang</label>
                    							<input class="form-control form-control-sm" value='<?php echo $user->nama_belakang; ?>' name='nama_belakang' readonly></input>
                    						</div>
                    					</div>
                    				</div>
                    				<div class="form-group">
                    					<label class="small">Username</label>
                    					<input class="form-control form-control-sm" type='text' name='username' value='<?php echo $user->username; ?>' autofocus></input>
                    				</div>
                                    <input type='hidden' name='password_lama' value='<?php echo $user->password; ?>'>
                    				<div class="form-row">
                    					<div class="col-md-6">
                    						<div class="form-group">
                    							<label class="small" for='password'>Masukkan Password <i style='color:green;'>*</i></label>
                    							<input class="form-control form-control-sm" type='password' name='password_baru' placeholder="Masukkan Password baru"></input>
                    						</div>
                    					</div>
                    					<div class="col-md-6">
                    						<div class="form-group">
                    							<label class="small" for='retype_password'>Tulis Ulang Password <i style='color:green;'>*</i></label>
                    							<input class="form-control form-control-sm" type='password' name='retype_password_baru' placeholder='Masukkan Kembali Password baru'></input>
                    						</div>
                    					</div>
                    				</div>
                                    <small><p class="text-sm-left text-muted">* dikosongkan tidak apa-apa</p></small>
                    				<button class="btn btn-success" type='submit'><i class="fas fa-save"></i> Simpan</button>
                                    <input type="hidden" name="log_id" value='<?php echo $kodeLog; ?>'>
                                    <input type='hidden' name='log_type' value='UPDATE'>
                                    <input type="hidden" name="kd_type" value='AKUN'>
                                    <input type='hidden' name='pesan_log' value='Akun Password Telah di Update'>
                    				</form>
                    			</div>
                    		</div>
                    	</div>
                    	<div class="col-md-6">
                    		<div class="card h-100">
                    			<div class="card-header">
                    				<i class="fas fa-chart-line"></i> Riwayat Penambahan Tabungan
                    			</div>
                    			<div class="card-body">
                    				<canvas id="UserChart" width='100%'></canvas>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
</body>

</html>