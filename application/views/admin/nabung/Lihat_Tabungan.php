<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('admin/_partials/head.php'); ?>
</head>

<body id="page-top">
	<?php $this->load->view('admin/_partials/header.php'); ?>
	<div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <?php $this->load->view('admin/_partials/navigation_bar.php'); ?>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                    <h1 class="mt-4">Mulai Menabung</h1>
                    <!-- BREADCRUMBS AREA -->
                    <?php $this->load->view('admin/_partials/breadcrumb.php'); ?>
                    <div class='card mb-4'>
                        <div class='card-header container-fluid'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <i class='fas fa-smile-wink'></i>
                                        Lihat Tabungan : 
                                    </div>
                                </div>
                            </div>
                            <div class='card-body'>
                                <div class='row'>
                                    <div class='col-md-12 text-center'>
                                    <h4>Lihat Tabungan <?php echo $nabung->kd_nabung; ?></h4>
                                    <div class="dropdown-divider"></div>
                                        <div class="row text-left">
                                            <div class="col-md-6">
                                                Kode Tabungan
                                            </div>
                                            <div class="col-md-6">
                                                <?php echo $nabung->kd_nabung; ?>
                                            </div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                        <div class="row text-left">
                                            <div class="col-md-6">
                                                Jumlah
                                            </div>
                                            <div class="col-md-6">
                                                <?php echo "Rp. " . number_format($nabung->nominal_uang,2,',','.'); ?>
                                            </div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                        <div class="row text-left">
                                            <div class="col-md-6">
                                                Ditambahkan Pada Tanggal
                                            </div>
                                            <div class="col-md-6">
                                                <?php echo $nabung->tgl_nabung; ?>
                                            </div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                        <div class="row text-left">
                                            <div class="col-md-6">
                                                Ditambahkan Oleh
                                            </div>
                                            <div class="col-md-6">
                                                <?php echo $userdata->nama_depan . " " . $userdata->nama_belakang; ?>
                                            </div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                    </div>
                                    <a class='btn btn-primary m-2' href='<?php echo base_url('admin/nabung'); ?>' title='Kembali ke menu sebelumnya'>
                                        <i class='fas fa-chevron-left'></i> Kembali</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
</body>

</html>