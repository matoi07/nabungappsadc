<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('admin/_partials/head.php'); ?>
</head>

<body id="page-top">
	<?php $this->load->view('admin/_partials/header.php'); ?>
	<div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <?php $this->load->view('admin/_partials/navigation_bar.php'); ?>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                    <h1 class="mt-4">Nabung</h1>
                    <!-- BREADCRUMBS AREA -->
                    <?php 
                    $this->load->view('admin/_partials/breadcrumb.php'); 
                    if($this->session->flashdata('success')):
                    ?>
                    <div class='alert alert-success' role='alert'>
                        <?php echo $this->session->flashdata('success'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class='fas fa-times'></i>
                        </button>
                    </div>
                    <?php
                    endif;
                    ?>
                    <div class='card mb-4'>
                        <div class='card-header container-fluid'>
                                <div class='row'>
                                    <div class='col-md-6'>
                                        <i class='fas fa-arrow-left'></i>
                                        <a href='<?php echo base_url('admin/') ?>'>Kembali</a>
                                    </div>
                                    <div class='col-md-6 float-right text-right'>
                                        <a href='<?php echo site_url('admin/nabung/add'); ?>'>Mulai Nabung</a>
                                        <i class='fas fa-arrow-right'></i>
                                    </div>
                                </div>
                            </div>
                            <div class='card-body'>
                            	<div class='table-responsive'>
                            		<table class='table table-hover' id='tableNabung' width="100%" cellspacing="0">
                            			<thead>
                            				<tr>
                                                <th>No. </th>
                            					<th>Nominal Uang</th>
                            					<th>Tgl. Nabung</th>
                            					<th>Action</th>
                            				</tr>
                            			</thead>
                            			<tbody>
                            			</tbody>
                            		</table>
                            	</div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#tableNabung").DataTable({
                    language:{
                        emptyTable: "Kamu belum pernah nabung? hayuk mulai nabung yuk.. <i class='fas fa-smile'></i>"
                    },
                    ajax:{
                        url: "<?php echo site_url('admin/Nabung/getAllNabung'); ?>"
                        type: 'GET'
                    },
                });
            });
        </script>
</body>

</html>