<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('admin/_partials/head.php'); ?>
</head>

<body id="page-top">
	<?php $this->load->view('admin/_partials/header.php'); ?>
	<div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <?php $this->load->view('admin/_partials/navigation_bar.php'); ?>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                    <h1 class="mt-4">Mulai Menabung</h1>
                    <!-- BREADCRUMBS AREA -->
                    <?php $this->load->view('admin/_partials/breadcrumb.php'); ?>
                    <div class='card mb-4'>
                        <div class='card-header container-fluid'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <i class='fas fa-smile-wink'></i>
                                        Yuk, Mulai Nabung
                                    </div>
                                </div>
                            </div>
                            <div class='card-body'>
                                <form action="<?php echo site_url('admin/nabung/add') ?>" method="post" enctype="multipart/form-data">
                                    <div class='form-row'>
                                        <div class='col-md-6'>
                                            <div class="form-group">     
                                                <label for="kd_nabung">Kode Nabung</label>
                                                <input class="form-control <?php echo form_error('kd_nabung') ? 'is-invalid':'' ?>"
                                                type="text" name="kd_nabung" value='<?php echo $kodeunik; ?>' readonly>
                                                <div class='invalid-feedback'>
                                                <?php echo form_error('kd_nabung'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <div class="form-group">     
                                                <label for="tgl_nabung">Tanggal Nabung</label>
                                                <input class="form-control <?php echo form_error('tgl_nabung') ? 'is-invalid':'' ?>"
                                                type="text" name="tgl_nabung" value="<?php echo date('Y-m-d'); ?>" readonly>
                                                <div class='invalid-feedback'>
                                                    <?php echo form_error('tgl_nabung'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        if(!empty($totaltabungan->id_totalnabung)){
                                    ?>
                                    <input type='hidden' name='id_totalnabung' value='<?php echo $totaltabungan->id_totalnabung; ?>'>
                                    <?php
                                        }else{
                                    ?>
                                    <input type="hidden" name="id_totalnabung" value="">
                                    <?php
                                        }
                                    ?>
                                    <input type='hidden' name='total_nabung' value='<?php echo $totaltabungan->total_nabung; ?>'>
                                    <input type='hidden' name='tabungansebelumnya' value='<?php echo $totaltabungan->tabungan_sebelumnya; ?>'>
                                    <input type='hidden' name='id_user' value='<?php echo $this->session->userdata('user_logged')->id_user; ?>'>
                                    <input type='hidden' name='log_id' value='<?php echo $kodeLog; ?>'>
                                    <input type='hidden' name='log_desc' value='Tabungan telah ditambahkan'>
                                    <input type='hidden' name='log_date' value='<?php echo date('Y-m-d H:i:s'); ?>'>
                                    <input type='hidden' name='log_type' value='TAMBAH'>
                                    <div class='form-group'>
                                        <label for='nominal_uang'>Nominal Uang</label>
                                        <input class='form-control <?php echo form_error('nominal_uang') ? 'is-invalid':'' ?>' type='number' min="0" name='nominal_uang' placeholder='Masukkan Nominal Uang' autofocus>
                                    </div>
                                    <button type='submit' class='btn btn-primary'><i class='fas fa-save'></i> Simpan</button>
                                    <a class='btn btn-danger' href='<?php echo base_url('admin/nabung/'); ?>'><i class='fas fa-ban'></i> Tidak Jadi</a>             
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
</body>

</html>