<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('admin/_partials/head.php'); ?>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <div class="card shadow-lg border-0 rounded-lg mt-5 mb-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">NabungApps Registrasi</h3></div>
                                    <div class="card-body">
                                        <form method='post' action=''>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for='namadepan' class='small mb-1'>Nama Depan <i style='color:red'>*</i></label>
                                                        <input type='text' name='namadepan' class='form-control form-control-sm <?php echo form_error('namadepan') ? 'is-invalid':'' ?>' placeholder='Masukkan Nama Depan' autofocus>
                                                        <div class="invalid-feedback">
                                                            <?php echo form_error('namadepan'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for='namabelakang'>Nama Belakang <i style='color:red'>*</i></label>
                                                        <input type='text' name='namabelakang' class="form-control form-control-sm <?php echo form_error('namabelakang') ? 'is-invalid':'' ?>" placeholder='Masukkan Nama Belakang'></input>
                                                        <div class="invalid-feedback">
                                                            <?php echo form_error('namabelakang'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for='username'>Username <i style='color:red'>*</i></label>
                                                <input class="form-control form-control-sm <?php echo form_error('username') ? 'is-invalid':'' ?>" type='text' name='username' placeholder='Masukkan Username'></input>
                                                <div class="invalid-feedback">
                                                    <?php echo form_error('username'); ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class='small mb-1' for='password'>Password 
                                                            <i style='color:red;'>*</i>
                                                        </label>
                                                        <input type="password" name="password" class='form-control form-control-sm <?php echo form_error('password') ? 'is-invalid':'' ?>' placeholder='Masukkan Password'>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1">Ketik Ulang Password
                                                            <i style='color:red;'>*</i>
                                                        </label>
                                                        <input type="password" name="password_match" class='form-control form-control-sm <?php echo form_error('password_match') ? 'is-invalid':'' ?>' placeholder='Ketik Ulang Password'>
                                                    </div>
                                                </div>
                                            </div>
                                            <small class="form-text text-muted">
                                                <p style='color:red;'>* Harus diisi</p>
                                            </small>
                                            <button type='submit' class='form-control btn btn-primary'><i class='far fa-edit'></i> Registrasi</button>
                                            <input type='hidden' name='id_user' value='<?php echo $kodeUser; ?>'>
                                            <input type='hidden' name='id_totaltabung' value='<?php echo $kodeTNB; ?>'>
                                            <input type='hidden' name='log_id' value='<?php echo $kodeLog; ?>'>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="<?php echo base_url('admin/user'); ?>">Sudah punya akun? Silahkan Login</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
    </body>
</html>
