<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('admin/_partials/head.php'); ?>
    </head>
    <body class="sb-nav-fixed">
        <?php $this->load->view('admin/_partials/header.php'); ?>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <?php $this->load->view('admin/_partials/navigation_bar.php'); ?>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4"><?php echo $title; ?></h1>
                        <!-- BREADCRUMBS AREA -->
                        <?php $this->load->view('admin/_partials/breadcrumb.php'); ?>
                        <!-- INCLUDE CARDS  -->
                        <?php $this->load->view('admin/_partials/cards_content.php'); ?>
                        <!-- INCLUDE Wishlist Cards -->
                        <!-- <?php $this->load->view('admin/_partials/wishlist_cards.php'); ?> -->
                    </div>
                    <?php
                        //var_dump($this->session->userdata('user_logged'));
                        //var_dump($menu);
                    ?>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
    </body>
</html>
