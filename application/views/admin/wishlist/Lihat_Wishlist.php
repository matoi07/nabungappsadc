<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('admin/_partials/head.php'); ?>
</head>

<body id="page-top">
	<?php $this->load->view('admin/_partials/header.php'); ?>
	<div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <?php $this->load->view('admin/_partials/navigation_bar.php'); ?>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                    <h1 class="mt-4">Mulai Menabung</h1>
                    <!-- BREADCRUMBS AREA -->
                    <?php $this->load->view('admin/_partials/breadcrumb.php'); ?>
                    <div class='card mb-4'>
                        <div class='card-header container-fluid'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <i class='fas fa-meteor'></i>
                                        Lihat Wishlist : 
                                    </div>
                                </div>
                            </div>
                            <div class='card-body'>
                                <div class='row'>
                                    <div class='col-md-6 text-center'>
                                    <h4>Lihat Wishlist <?php echo $wishlist->id_wishlist; ?></h4>
                                    <div class="dropdown-divider"></div>
                                        <div class="row text-left">
                                            <div class="col-md-6">
                                                ID Wishlist
                                            </div>
                                            <div class="col-md-6">
                                                <?php echo $wishlist->id_wishlist; ?>
                                            </div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                        <div class="row text-left">
                                            <div class="col-md-6">
                                                Nama Item
                                            </div>
                                            <div class="col-md-6">
                                                <?php echo $wishlist->nama_item; ?>
                                            </div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                        <div class="row text-left">
                                            <div class="col-md-6">
                                                Jumlah barang x Harga Barang
                                            </div>
                                            <div class="col-md-6">
                                                <?php echo $wishlist->jml_item . " x " . number_format($wishlist->nominal_wishlist,2,",","."); ?>
                                            </div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                        <div class="row text-left">
                                            <div class="col-md-6">
                                                Total Biaya Barang
                                            </div>
                                            <div class="col-md-6">
                                                <?php echo "Rp. " . number_format($wishlist->jml_total,2,",","."); ?>
                                            </div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                        <div class="row text-left">
                                            <div class="col-md-6">
                                                Status
                                            </div>
                                            <div class="col-md-6">
                                                <?php
                                                    switch ($wishlist->status){
                                                        case 'pending':
                                                            echo "<i class='far fa-clock' style='color: #ffc107;'></i> Pending";
                                                            break;
                                                        case 'diterima':
                                                            echo "<i class='far fa-check-circle' style='color: #28a745;'></i> Diterima";
                                                            break;
                                                        case 'ditolak':
                                                            echo "<i class='far fa-times-circle' style='color: #dc3545;'></i> Ditolak";
                                                            break;
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                    </div>
                                    <div class="col-md-6 text-center">
                                        <h4>Picture</h4>
                                        <div class="dropdown-divider"></div>
                                        <a href='<?php echo base_url('dist/assets/upload/wishlist/'.$wishlist->wishlist_img.'') ?>' title='<?php echo $wishlist->nama_item; ?>' target='_blank'>
                                            <img src='<?php echo base_url('dist/assets/upload/wishlist/'.$wishlist->wishlist_img.'') ?>' width='120px' height='120px'>
                                        </a>
                                    </div>
                                        <a class='btn btn-primary m-2' href='<?php echo base_url('admin/wishlist/'); ?>' title='Kembali ke menu sebelumnya'>
                                            <i class='fas fa-chevron-left'></i> Kembali
                                        </a>
                                        <form method="post" action="<?php echo base_url('admin/wishlist/action'); ?>">
                                        <button class="btn btn-success m-2" name="terima" id="terima" value="terima">
                                            <i class='fas fa-check'></i> Terima
                                        </button>
                                        <button class="btn btn-danger m-2" name="tolak" id="tolak" value="tolak">
                                            <i class='fas fa-times'></i> Tolak
                                        </button>
                                        <input type="hidden" name="id_wishlist" value='<?php echo $wishlist->id_wishlist; ?>'>
                                        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
</body>

</html>