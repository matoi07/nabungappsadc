<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('admin/_partials/head.php'); ?>
</head>

<body id="page-top">
	<?php $this->load->view('admin/_partials/header.php'); ?>
	<div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <?php $this->load->view('admin/_partials/navigation_bar.php'); ?>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                    <h1 class="mt-4">Nabung</h1>
                    <!-- BREADCRUMBS AREA -->
                    <?php 
                    $this->load->view('admin/_partials/breadcrumb.php'); 
                    if($this->session->flashdata('success')):
                    ?>
                    <div class='alert alert-success' role='alert'>
                        <?php echo $this->session->flashdata('success'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class='fas fa-times'></i>
                        </button>
                    </div>
                    <?php
                    endif;
                    if($this->session->flashdata('deleteSuccess')):
                    ?>
                    <div class="alert alert-success" role='alert'>
                        <?php 
                            echo $this->session->flashdata('deleteSuccess');
                        ?>
                        <button type="button" class="close" data-dismiss="alert" aria_label="Close">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                    <?php
                    endif;
                    if($this->session->flashdata('TerimaSuccess')):
                    ?>
                    <div class="alert alert-success" role="alert">
                        <?php 
                            echo $this->session->flashdata('TerimaSuccess');
                        ?>
                        <button type="button" class="close" data-dismiss="alert" aria_label="Close">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                    <?php
                    endif; 
                    if($this->session->flashdata('TolakSuccess')):
                    ?>
                    <div class="alert alert-danger" role="alert">
                        <?php 
                            echo $this->session->flashdata('TolakSuccess');
                        ?>
                        <button type="button" class="close" data-dismiss="alert" aria_label="Close">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                    <?php
                    endif; 
                    ?>
                    <div class='card mb-4'>
                        <div class='card-header container-fluid'>
                                <div class='row'>
                                    <div class='col-md-6'>
                                        <a href='<?php echo base_url('admin/') ?>'>
                                            <i class='fas fa-arrow-left'></i> Kembali
                                        </a>
                                    </div>
                                    <div class='col-md-6 float-right text-right'>
                                        <a href='<?php echo site_url('admin/wishlist/add'); ?>'>
                                            Tambah Wishlist <i class='fas fa-arrow-right'></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class='card-body'>
                            	<div class='table-responsive'>
                            		<table class='table table-hover' id='dataTable' width="100%" cellspacing="0">
                            			<thead>
                            				<tr>
                                                <th>No. </th>
                            					<th>Nama</th>
                            					<th>Nominal</th>
                                                <th>Status</th>
                            					<th>Action</th>
                            				</tr>
                            			</thead>
                            			<tbody>
                            				<?php $i = 1; foreach ($wishlist as $wish): ?>
                            				<tr>
                                                <td><?php echo $i; ?></td>
                            					<td><?php echo $wish->nama_item; ?></td>
                            					<td><?php echo "Rp. " . number_format($wish->nominal_wishlist,2,',','.'); ?></td>
                                                <td><?php echo $wish->status; ?></td>
                            					<td>
                                                    <a href="<?php echo site_url('admin/wishlist/edit/' . $wish->id_wishlist) ?>" class="btn btn-primary btn-sm" title="Ubah"><i class="fas fa-edit"></i></a>
                                                    <a href='<?php echo site_url('admin/wishlist/view/' . $wish->id_wishlist) ?>' class="btn btn-success btn-sm" title="Lihat"><i class='fas fa-eye'></i></a>
                                                    <a href="<?php echo site_url('admin/wishlist/delete/'. $wish->id_wishlist) ?>" class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></a>
                                                </td>
                            				</tr>
                            				<?php $i++; endforeach; ?>
                            			</tbody>
                            		</table>
                            	</div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
</body>
</html>