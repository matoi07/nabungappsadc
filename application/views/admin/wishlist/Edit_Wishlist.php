<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('admin/_partials/head.php'); ?>
</head>

<body id="page-top">
	<?php $this->load->view('admin/_partials/header.php'); ?>
	<div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <?php $this->load->view('admin/_partials/navigation_bar.php'); ?>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                    <h1 class="mt-4">Mulai Menabung</h1>
                    <!-- BREADCRUMBS AREA -->
                    <?php $this->load->view('admin/_partials/breadcrumb.php'); ?>
                    <div class='card mb-4'>
                        <div class='card-header container-fluid'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <i class='fas fa-meteor'></i>
                                        Edit Wishlist : 
                                    </div>
                                </div>
                            </div>
                            <div class='card-body'>
                                <form action="" method="post" enctype="multipart/form-data">
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">     
                                                <label for="id_wishlist">Id Wishlist</label>
                                                <input class="form-control <?php echo form_error('id_wishlist') ? 'is-invalid':'' ?>"
                                                type="text" name="kd_wishlist" value='<?php echo $wishlist->id_wishlist; ?>' readonly>
                                                <div class='invalid-feedback'>
                                                    <?php echo form_error('id_wishlist'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for='nama_item'>Nama Item</label>
                                                <input type='text' class='form-control <?php echo form_error('nama_item') ? 'is-invalid':'' ?>' type='text' name='nama_item' value='<?= $wishlist->nama_item; ?>' autofocus>
                                            </div>
                                        </div>
                                    </div>   
                                    <div class='form-row'>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for='nominal_item'>Nominal Item</label>
                                                <input type='number' min='0' class='form-control <?php echo form_error('nominal_item') ? 'is-invalid':'' ?>' type='text' name='nominal_item' value='<?= $wishlist->nominal_wishlist; ?>'>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for='jml_item'>Jumlah Item</label>
                                                <input type='number' min='0' class='form-control <?php echo form_error('jml_item') ? 'is-invalid':'' ?>' value='<?php echo $wishlist->jml_item; ?>' name='jml_item'>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for='foto'>Foto</label>
                                        <input class="form-control-file <?php echo form_error('price') ? 'is-invalid':'' ?>" type="file" name="image">
                                        <input type='hidden' name='old_link_gambar' value='<?php echo $wishlist->wishlist_img; ?>'>
                                    </div>
                                    <div class="form-group">
                                        <label for="deskripsi">Deskripsi</label>
                                        <textarea class='form-control <?php echo form_error('deskripsi') ? 'is-invalid':'' ?>' name='deskripsi'><?php echo $wishlist->deskripsi_wishlist; ?>
                                        </textarea>
                                    </div>
                                    <input type='hidden' name='id_user' value='<?php echo $this->session->userdata('user_logged')->id_user; ?>'>
                                    <input type='hidden' name='log_id' value='<?php echo $kodeLog; ?>'>
                                    <input type='hidden' name='log_desc' value='Wishlist telah diubah'>
                                    <input type='hidden' name='log_date' value='<?php echo date('Y-m-d H:i:s'); ?>'>
                                    <input type='hidden' name='log_type' value='UPDATE'>
                                    <button type='submit' class='btn btn-primary'><i class='fas fa-save'></i> Simpan</button>
                                    <a class='btn btn-danger' href='<?php echo base_url('admin/wishlist/'); ?>'><i class='fas fa-ban'></i> Tidak Jadi</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
</body>

</html>