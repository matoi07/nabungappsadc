<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('admin/_partials/head.php'); ?>
</head>

<body id="page-top">
	<?php $this->load->view('admin/_partials/header.php'); ?>
	<div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <?php $this->load->view('admin/_partials/navigation_bar.php'); ?>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                    <h1 class="mt-4">Tambah Wishlist</h1>
                    <!-- BREADCRUMBS AREA -->
                    <?php $this->load->view('admin/_partials/breadcrumb.php'); ?>
                    <div class='card mb-4'>
                        <div class='card-header container-fluid'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <i class='fas fa-meteor'></i>
                                        Yuk, tambahkan apa yang kamu inginkan 
                                    </div>
                                </div>
                            </div>
                            <div class='card-body'>
                                <form action="<?php echo site_url('admin/wishlist/add') ?>" method="post" enctype="multipart/form-data">
                                    <div class='form-row'>
                                        <div class='col-md-6'>
                                            <div class="form-group">     
                                                <label for="kd_wishlist">Kode Wishlist</label>
                                                <input class="form-control <?php echo form_error('kd_wishlist') ? 'is-invalid':'' ?>"
                                                type="text" name="kd_wishlist" value='<?php echo $kodeunik; ?>' readonly>
                                                <div class='invalid-feedback'>
                                                <?php echo form_error('kd_wishlist'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <label for='nama_wishlist'>Nama Item</label>
                                                <input class='form-control <?php echo form_error('nama_wishlist') ? 'is-invalid':'' ?>' type='text' name='nama_wishlist' placeholder='Masukkan Nama Item' autofocus>
                                                <div class="invalid-feedback">
                                                    <?= form_error('nama_wishlist'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='form-row'>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for='nominal_item'>Nominal Item</label>
                                                <input class='form-control <?php echo form_error('nominal_item') ? 'is-invalid':'' ?>' type='number' min='0' placeholder='Masukkan Nominal Item' name='nominal_item'>
                                                <div class="invalid-feedback">
                                                    <?php echo form_error('nominal_item'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <div class="form-group">
                                                <label for='jml_item'>Jumlah Item</label>
                                                <input class='form-control <?php echo form_error('jml_item') ? 'is-invalid':'' ?>' type='number' min='0' name='jml_item' placeholder='Masukkan Jumlah Item'>
                                                <div class="invalid-feedback">
                                                    <?= form_error('jml_item'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="image">Foto</label>
                                        <input class="form-control-file" type="file" name="image" />
                                        <div class="invalid-feedback">
                                            <?php echo form_error('image'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for='deskripsi'>Deskripsi</label>
                                        <textarea class='form-control' name='deskripsi' placeholder='Masukkan Deskripsi Singkat'></textarea>
                                    </div>
                                    <input type='hidden' name='id_user' value='<?php echo $this->session->userdata('user_logged')->id_user; ?>'>
                                    <input type='hidden' name='log_id' value='<?php echo $kodeLog; ?>'>
                                    <input type='hidden' name='log_desc' value='Wishlist telah dibuat'>
                                    <input type='hidden' name='log_date' value='<?php echo date('Y-m-d H:i:s'); ?>'>
                                    <input type='hidden' name='log_type' value='TAMBAH'>
                                    <button type='submit' class='btn btn-primary'><i class='fas fa-save'></i> Simpan</button>
                                    <a class='btn btn-danger' href='<?php echo base_url('admin/wishlist/'); ?>'><i class='fas fa-ban'></i> Tidak Jadi</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view('admin/_partials/footer.php'); ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/_partials/js.php'); ?>
</body>

</html>