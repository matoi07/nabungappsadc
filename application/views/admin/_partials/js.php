<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('dist/js/scripts.js') ?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
        <!--<script src="<?php echo base_url('dist/assets/demo/chart-area-demo.js'); ?>"></script>
        <script src="<?php echo base_url('dist/assets/demo/chart-bar-demo.js'); ?>"></script>-->
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('dist/assets/demo/datatables-demo.js'); ?>"></script>
        <script type="text/javascript">
        var ctx = document.getElementById("UserChart").getContext('2d');
        var chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels:[
                    <?php
                        foreach($chart as $item){
                            echo "'" . $item->bulan . "',";
                        }
                    ?>
                ],
                datasets:[{
                    label: "History Tabungan",
                    backgroundColor: 'rgba(40, 167, 69, 0.8)',
                    borderColor: 'rgba(23, 99, 40, 0.8)',
                    data:[
                        <?php
                            foreach($chart as $item){
                                echo "'" . $item->total . "',";
                            }
                        ?>
                    ],
                }]
            },
            options:{
            	scales:{
            		xAxes:[{
            			ticks:{
            				maxTicksLimit: 6,
            			}
            		}],
            		yAxes:[{
            			ticks:{
            				min: 0,
            				maxTicksLimit: 5,
            			}
            		}]
            	}
            }
        });
    </script>