<div class="sb-sidenav-menu">
    <div class="nav">
        <div class="sb-sidenav-menu-heading">Dashboard</div>
        <a class="nav-link" href="<?php echo site_url('admin/'); ?>">
            <div class="sb-nav-link-icon">
                <i class="fas fa-tachometer-alt"></i>
                Dashboard
            </div>
        </a>
        <a class="nav-link" href="<?php echo site_url('admin/Tarik'); ?>">
            <div class="sb-nav-link-icon">
                <i class="fas fa-money-bill-alt"></i>
                Tarik Uang
            </div>
        </a>
    </div>
</div>

<div class="sb-sidenav-footer">
    <div class="small">Login Sebagai :</div>
    <?php echo $this->session->userdata('user_logged')->username; ?>
</div>