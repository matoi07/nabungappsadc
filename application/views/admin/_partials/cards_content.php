                       <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-success text-white mb-4">
                                    <div class='card-header align-items-center justify-content-between'>
                                        Total Tabungan
                                    </div>
                                    <?php
                                    if(!empty($totalnabung->total_nabung)){
                                    ?>
                                        <div class="card-body font-weight-bold">
                                            <?php echo "Rp. " . number_format($totalnabung->total_nabung,2,",",".");?>
                                        </div>
                                    <?php
                                        }else{
                                        ?>
                                            <div class="card-body font-weight-bold">
                                                <?php echo "Rp. ". number_format(0,2,",","."); ?>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class='card-header align-items-center justify-content-between'>
                                        Total Penambahan Hari Ini
                                    </div>
                                    <div class="card-body font-weight-bold"><?php echo "Rp. " . number_format($totalharian->nominal_uang,2,",","."); ?></div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-danger text-white mb-4">
                                    <div class='card-header align-items-center justify-content-between'>
                                        Total Pengeluaran
                                    </div>
                                    <?php
                                    if(!empty($totalnabung->total_pengeluaran)){
                                    ?>
                                        <div class="card-body font-weight-bold">
                                            <?php echo "Rp. " . number_format($totalnabung->total_pengeluaran,2,',','.'); ?>
                                        </div>
                                    <?php
                                    }else{
                                    ?>
                                        <div class="card-body font-weight-bold">
                                            <?php echo "Rp. " . number_format(0,2,',','.'); ?>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>