<div class="row">
	<div class="col-md-5">
		<div class="card-deck">
			<?php
				foreach($wishlist as $wish){
			?>
			<div class="card">
				<?php if(!empty($wish->wishlist_img))
				{
				?>
					<a href="<?php echo site_url('admin/wishlist/view/'.$wish->id_wishlist.''); ?>">
						<img class="card-img-top" src="<?php echo base_url('dist/assets/upload/wishlist/'.$wish->wishlist_img.""); ?>">
					</a>
				<?php
				}
				?>
				<div class="row">
					<div class="col-sm-12">
						<div class="card-body">
							<div class="row">
								<h5 class="card-title" style="font-size:1.0rem;">
									<?php echo $wish->nama_item; ?>
								</h5>
							</div>
						</div>
						<div class="card-footer">
							<a href='#'>test</a>
						</div>
					</div>
				</div>
			</div>
			<?php
				}
			?>
		</div>
	</div>
</div>